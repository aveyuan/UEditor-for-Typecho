# UEditor for Typecho  
更新时间 2017年11月13日  
使用1.4.3.3版本  
更新了17年8月的补丁  
克隆下来使用的时候请修改外部文件名为UEditor

## 推荐
这里推荐一下我的微微CMS系统，历经3年多的时间，不间断更新打造而成。  
特点:轻量，快速，开箱即用，高性能，采用Golang开发  
官网地址:https://www.vvcms.cn  
演示地址:http://demo.vvcms.cn/admin  
账号:admin  密码:123456  

